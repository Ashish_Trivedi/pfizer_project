import win32com.client as win32
from win32com.client import constants
from shutil import copyfile
import uuid
import PyPDF2
import re
import pymongo
import datetime
from config import collection_name, database_name, mongodb_uri, user_name, password
from bson.objectid import ObjectId



def font_formatter_v2(src_filepath, template_path, country_name,reference_filepath, file_id, project_id):	
	myclient = pymongo.MongoClient(mongodb_uri, username=user_name, password=password, authSource=database_name)
	mydb = myclient[database_name]
	mycol = mydb[collection_name]
	
	word = win32.gencache.EnsureDispatch('Word.Application')
	word.Visible = True
	# copy_filepath = "C:\\Users\\G_Kumar_DF\\Desktop\\lpd_copy4.docx"
	# copyfile(src_filepath, copy_filepath)	
	doc = word.Documents.Open(src_filepath)
	doc.Activate()
	activeDoc = word.ActiveDocument
	if country_name == "Bosnia":
		font_template_details = font_details_extractor_bosnia(template_pdf_path=reference_filepath, font_page_no= 0)
	elif country_name == "Saudi Arabia":
		font_template_details = font_details_extractor_saudi(template_pdf_path=template_path, font_page_no = 6)
	else:
		raise ValueError("Country name not valid")
	label_list = []
	for word_t in doc.Words:
	#     print(word_t)
	    word_dict = {}
	    word_dict['Text'] = str(word_t)
	    word_dict['Font'] = word_t.Font.Name
	    word_dict['Size'] = int(word_t.Font.Size)
	    if word_t.Italic == -1:
	        word_dict["is_italics"] = True
	    else:
	        word_dict["is_italics"] = False
	    if word_t.Bold == -1:
	        word_dict["is_bold"] = True
	    else:
	        word_dict["is_bold"] = False
	    if word_t.Underline == 1:
	        word_dict["is_underlined"] = True
	    else:
	        word_dict["is_underlined"] = False
	    label_list.append(word_dict)


	# font size conflict accumulation
	i = 0
	label_list_len = len(label_list)
	font_size_conflicts = list()
	while(i<label_list_len):
		conflict_range = dict()
		comment_text_list = list()
		if label_list[i]["Size"] != font_template_details["Size"]:
			comment_text_list.append(label_list[i]["Text"])
			start_index = i
			end_index = i
			while((i+1<label_list_len) and (label_list[i+1]["Size"] != font_template_details["Size"])):
				comment_text_list.append(label_list[i+1]["Text"])
				i = i + 1
				end_index = i
			comment_text = " ".join(comment_text_list)
			conflict_range["type_conflict"] = "FONT_SIZE"
			conflict_range["comment_text"] = comment_text
			conflict_range["start_index"] = start_index
			conflict_range["end_index"] = end_index
			if conflict_range["comment_text"] != "\r\x07" and conflict_range["comment_text"] != "\r":
				font_size_conflicts.append(conflict_range)
		i = i + 1

	# font name conflict accumulation
	font_name_conflicts = list()
	i=0
	while(i<label_list_len):
		conflict_range = dict()
		comment_text_list = list()
		if label_list[i]["Font"] != font_template_details["Font"]:
			comment_text_list.append(label_list[i]["Text"])
			start_index = i
			end_index = i
			while((i+1<label_list_len) and (label_list[i+1]["Font"] != font_template_details["Font"])):
				comment_text_list.append(label_list[i+1]["Text"])
				i = i + 1
				end_index = i

			comment_text = " ".join(comment_text_list)
			conflict_range["type_conflict"] = "FONT_NAME"
			conflict_range["comment_text"] = comment_text
			conflict_range["start_index"] = start_index
			conflict_range["end_index"] = end_index
			if conflict_range["comment_text"] != "\r":
				font_name_conflicts.append(conflict_range)
		i = i + 1
	comments = []

	for i in range(len(font_size_conflicts)):
		temp_comment = {}
		comment_text = "font size mismatch: recommended: {}".format(font_template_details['Size'])
		activeDoc, start_index, end_index, target_text = add_comment(activeDoc, font_size_conflicts[i]["start_index"], font_size_conflicts[i]["end_index"], comment_text, font_size_conflicts[i]["comment_text"])
		temp_comment["recommendations"] = {}
		temp_comment["recommendations"]["font_size"] = font_template_details['Size']
		temp_comment["recommendations"]["font_name"] = None
		temp_comment["comment_text"] = comment_text
		temp_comment["conflict_type"] = "FONT_SIZE"
		temp_comment["comment_id"] = str(uuid.uuid1())
		temp_comment["reference_doc"] =  reference_filepath
		temp_comment["start_index"] = start_index
		temp_comment["end_index"] = end_index
		temp_comment["file_id"] = file_id
		temp_comment["target_text"] = target_text
		temp_comment["project_id"] = project_id
		temp_comment["updated_at"] = None
		dt = datetime.datetime.now().isoformat()
		temp_comment["created_at"] = dt
		temp_comment["action"] = None
		temp_comment["action_on"] = None
		temp_comment["action_by"] = None
		temp_comment["_deleted"] = False
		comments.append(temp_comment)

	for i in font_name_conflicts:
		comment_text = i["comment_text"]

		adder, subtractor = index_fixer(comment_text)
		i["start_index"] = i["start_index"] + adder
		i["end_index"] = i["end_index"] - subtractor
		if i["start_index"] > i["end_index"] and subtractor == 0:
			i["start_index"] = i["end_index"]
			i["end_index"] = i["end_index"]
		elif i["end_index"] < i["start_index"] and adder == 0:
			i["start_index"] = i["start_index"]
			i["end_index"] = i["start_index"]
	for i in range(len(font_name_conflicts)):
		temp_comment = {}
		comment_text = "font name mismatch: recommended: {}".format(font_template_details['Font'])
		activeDoc, start_index, end_index, target_text = add_comment(activeDoc, font_name_conflicts[i]["start_index"], font_name_conflicts[i]["end_index"], comment_text, font_name_conflicts[i]["comment_text"])
		temp_comment["recommendations"] = {}
		temp_comment["recommendations"]["font_size"] = None
		temp_comment["recommendations"]["font_name"] = font_template_details['Font']
		temp_comment["comment_text"] = comment_text
		temp_comment["conflict_type"] = "FONT_NAME"
		temp_comment["comment_id"] = str(uuid.uuid1())
		temp_comment["reference_doc"] =  reference_filepath
		temp_comment["start_index"] = start_index
		temp_comment["end_index"] = end_index
		temp_comment["file_id"] = file_id
		temp_comment["target_text"] = target_text
		temp_comment["project_id"] = project_id
		temp_comment["updated_at"] = None
		dt = datetime.datetime.now().isoformat()
		temp_comment["created_at"] = dt
		temp_comment["action"] = None
		temp_comment["action_on"] = None
		temp_comment["action_by"] = None
		temp_comment["_deleted"] = False
		comments.append(temp_comment)

	# if len(comments):
		# object_id = ObjectId(project_id)
		# inserted = mycol.update_one({"_id": object_id}, {"$push" : {"conflicts.comments": comments}})
		# print("Saved to database")
		# removes the ids
		# comment_ids = list(map(lambda x: x.pop("_id", None), comments))
	activeDoc.Save()
	doc.Close(False)
	word.Quit()
	return src_filepath, comments

'''
Reads the PDF file page-wise
Returns: PDF content in a list of pages
'''
def read_pdf(pdf_path):
    pages = []
    pdf_file = pdf_path
    read_pdf = PyPDF2.PdfFileReader(pdf_file)
    number_of_pages = read_pdf.getNumPages()

    for page_number in range(number_of_pages):
        page = read_pdf.getPage(page_number).extractText()
        reg = re.compile(r"Commented.*\n.*")
        found_comments = reg.findall(page)
        for comment in found_comments:
            page = page.replace(comment, "")

        pages.append(page)
    return pages

'''
Strips the text
'''
def cleaner(text):
    text = text.strip()
    if len(text)>0:
        return text
    else:
        return None

'''
Font format details - Dictionary Generator
'''
def font_details_extractor_bosnia(template_pdf_path, font_page_no):
	template_path = template_pdf_path
	template_pages = read_pdf(template_path)
	font_page = template_pages[font_page_no]
	font_text = font_page[font_page.index("FONT:")+len("FONT:"):font_page.index("LANGUAGE")]
	font_text_list = font_text.split("\n \n")
	def cleaner(text):
		text = text.strip()
		if len(text)>0:
			return text
		else:
			return None
	font_list = []
	for text in font_text_list:
		text = cleaner(text)
		if text:
			font_list.append(text)
	rule_list = list(font_list[0].split("\n"))
	font_details_dict = dict()
	rule_list = filter(None, rule_list)
	for font_prop in rule_list:
		if(":" in font_prop):
			key, value = font_prop.split(":")
			key, value = key.strip(), value.strip()
			if key == "Size":
				value = int(value)
			font_details_dict[key] = value
	return font_details_dict




def font_details_extractor_saudi(template_pdf_path, font_page_no):
	# font_page_no = 0
	# if country_name == "Bosnia":
	# 	font_page_no = 0
		# need to fixed later
		# return {'Font': 'Times New Roman',
 		# 		'Size': 11,
 		# 		'Font style': 'Regular',
 		# 		'Character spacing': 'Normal',
 		# 		'Font colour': 'Black (i.e. the text throughout the annexes should be \npresented in black font, including figures, tables, \npictograms, \netc.).'}
	# elif country_name == "Saudi Arabia":
	# 	font_page_no = 6
	# else:
	# 	raise ValueError("Country name not valid")

	template_pages = read_pdf(template_pdf_path)
	font_page = template_pages[font_page_no]
	font_text = font_page[font_page.index("FONT:")+len("FONT:"):font_page.index("LANGUAGE")]
	font_text_list = font_text.split("\n \n")

	font_list = []
	for text in font_text_list:
		text = cleaner(text)
		if text:
			font_list.append(text)

	font_details_dict = dict()

	for font_prop in font_list:
		key, value = font_prop.split(":")
		key, value = key.strip(), value.strip()
		if key == "Size":
			value = int(value)
		font_details_dict[key] = value

	return font_details_dict


'''
Adds comment in the document
'''
def add_comment(activeDoc, start_index, end_index, comment_text, text_element):
	print("Adding commment for font and formatting at index {}".format(start_index))
	conflict_range = None
	try:
		start_index = start_index + 1
		end_index = end_index + 1
		if start_index > end_index:
			start_index = end_index
		elif end_index < start_index:
			end_index = start_index
		if start_index == end_index:
			if text_element.strip() == "\r\x07":
				return activeDoc, None, None, None
			else:
				conflict_range = activeDoc.Words(start_index)
		else:
			conflict_range = activeDoc.Range(activeDoc.Words(start_index).Start, activeDoc.Words(end_index).End) 
		_ = activeDoc.Comments.Add(conflict_range, comment_text)
		txt = conflict_range.Text
	except Exception as e:
		print(start_index, end_index)
		print("Exception",e)
		pass

		# Incase of error start_index and end_index will be empty
		return activeDoc, None, None, None
        # print("start_index", start_index)
        # print("end_index", end_index)
        # print(comment_text)
		pass
	return activeDoc, start_index, end_index, txt

def index_fixer(s):
    s_arr = s.split("\r\x07")
    adder = 0
    subtracter = 0
    visited = False
    for i in s_arr:
        if not len(i) and not visited:
            adder = adder + 1
        else:
            visited = True
        if visited and not len(i):
            subtracter = subtracter + 1

    return adder, subtracter