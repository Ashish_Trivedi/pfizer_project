def apply_changes(word, doc, start_index, end_index, recommended_font_size, recommended_font_name):
    print("Applying changes at index {}".format(start_index))
    doc.Activate()
    activeDoc = word.ActiveDocument
    conflict_range = None
    if start_index == end_index:
	    conflict_range = activeDoc.Words(start_index)
    else:
        conflict_range = activeDoc.Range(activeDoc.Words(start_index).End, activeDoc.Words(end_index).End) 
    
    if recommended_font_name:
        conflict_range.Font.Name = recommended_font_name
    elif recommended_font_size:
        conflict_range.Font.Size = recommended_font_size
    return activeDoc



def delete_comments(doc, list_comment):
    # Delete comment code - needed to be added and also merge these features with master repo
    for item in list_comment:
        comment_text = item["target_text"]
        for cmnt in doc.Comments:
            if(cmnt.Scope.Text==comment_text):
                cmnt.DeleteRecursively()
    return doc