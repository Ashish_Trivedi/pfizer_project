import json
import cherrypy
import pythoncom
import comtypes.client
import os
import random
import string
import os.path
from cherrypy.lib import static
import ntpath

class Wordtopdf(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def index(self):
        result = {"message": "conversion done"}
        if cherrypy.request.method == "POST":
            infile = cherrypy.request.json['file_path']
            file_name = ntpath.basename(infile)
            outfile = os.path.splitext(file_name)[0]
            outfile_dir = os.path.splitext(infile)[0]
            # print("------------------",outfile_dir)
            # print("@@@@@@@@@@@@@",outfile_dir+".pdf")
            wdFormatPDF = 17
            word = comtypes.client.CreateObject('Word.Application')
            doc = word.Documents.Open(infile)
            doc.SaveAs(outfile_dir+".pdf", FileFormat=wdFormatPDF)
            # print("reached here")
            doc.Close(False)
            word.Quit()
            return result


def onThreadStart(threadIndex):
		pythoncom.CoInitialize()

cherrypy.config.update({'server.socket_port': 3009})
cherrypy.config.update({
    'global': {
       'engine.autoreload.on' : False,
       'server.socket_host': '0.0.0.0',
       'server.thread_pool' : 1
     }
 })

if __name__ == '__main__':
	cherrypy.engine.subscribe('start_thread', onThreadStart, )
	cherrypy.quickstart(Wordtopdf(), '/')