import win32com.client
import pickle
from win32com.client import constants
import _pickle as pickle
import gc
import uuid
import datetime
import pymongo
from config import mongodb_uri, database_name, collection_name, user_name, password
from bson.objectid import ObjectId

def add_comment(activeDoc, start_index,end_index, comment_text):
    print("Adding comment - spell grammar at index: {}".format(start_index))
    conflict_range = activeDoc.Range(start_index,end_index)
    _ = activeDoc.Comments.Add(conflict_range, comment_text)
    return activeDoc, comment_text

def rng_to_spell(rng):
        d = {'text': rng.Text, 'start': rng.Start, 'end': rng.End, 'suggestions': [],'type_of_language_error':'SPELL_ERROR'}
        for i in range(1, rng.GetSpellingSuggestions().Count+1):
            d['suggestions'].append(rng.GetSpellingSuggestions().Item(i).Name)
        return d       

def rng_to_grammar(rng):
        d = {'text': rng.Text, 'start': rng.Start, 'end': rng.End, 'suggestions': [],'type_of_language_error':'GRAMMAR_ERROR'}
        for i in range(1, rng.GetSpellingSuggestions().Count+1):
            d['suggestions'].append(rng.GetSpellingSuggestions().Item(i).Name)
        return d

def spell_grammar_errors(medical_data_filepath , lpd_filepath,ref_doc_path, file_id, project_id):
    
    with open(medical_data_filepath, 'rb') as medict:
            Medical_data = pickle.load(medict)
    
    myclient = pymongo.MongoClient(mongodb_uri, username=user_name, password=password, authSource=database_name)
    mydb = myclient[database_name]
    mycol = mydb[collection_name]
    error_conflicts_comments = []

    word = win32com.client.gencache .EnsureDispatch("Word.Application")
    word.Visible = True
    doc = word.Documents.Open(lpd_filepath)
    doc.Activate()
    activeDoc = word.ActiveDocument
    error_dict={}
    error_list=[]
    drug_name_list = ['Dynastat','Minidiab','Efexor','XR','Telzenna','Celebrex','Neurontin','Nimenrix','Sutent']
    corrections = { 'spelling': [],'grammar':[],'spellingcount': 0 }
    Before_med_term=doc.Range().SpellingErrors.Count
    for rng in doc.Range().GrammaticalErrors:
        corrections['grammar'].append(rng_to_grammar(rng))   
    for rng in doc.Range().SpellingErrors:
        if(rng.Text not in Medical_data and rng.Text not in drug_name_list ):
            corrections['spelling'].append(rng_to_spell(rng))
            error_list.append(rng.Text)
    count=1
    g_count=1
    errors=[]
    for correction in corrections['spelling']:
        if correction['type_of_language_error']=='SPELL_ERROR':
            comment = ""
            comment_dict={}
            target_text =""
            if len(correction['suggestions']):
                comment = "Type of language error: {}.\nSuggestions: {}".format(correction['type_of_language_error'], ", ".join(correction["suggestions"]))
                comment_dict["target_text"] = correction["text"]
                comment_dict["comment_text"] = comment
                comment_dict["conflict_type"] = "GRAMMAR_SPELLING"
                comment_dict["suggestions"] = correction["suggestions"]
                comment_dict["comment_id"] = str(uuid.uuid1())
                comment_dict["reference_doc"] = ref_doc_path,
                comment_dict["type_of_language_error"] = correction["type_of_language_error"]
            else:
                comment = "Type of language error: {}.\nNo suggestions".format(correction["type_of_language_error"])
                comment_dict["target_text"] = correction["text"]
                comment_dict["comment_text"] = comment
                comment_dict["conflict_type"] = "GRAMMAR_SPELLING"
                comment_dict["suggestions"] = []
                comment_dict["comment_id"] = str(uuid.uuid1())
                comment_dict["reference_doc"] = ref_doc_path,
                comment_dict["type_of_language_error"] = correction["type_of_language_error"]
            errors.append(comment_dict)
            if count==1:
               activeDoc, target_text =  add_comment(activeDoc, correction["start"], correction["end"],comment)
            else:
                activeDoc, target_text = add_comment(activeDoc, correction["start"]+count-1, correction["end"]+count-1,comment)
            
            temp_comment = {}
            temp_comment["recommendations"] = comment_dict["comment_text"]
            temp_comment["comment_text"] = comment_dict["comment_text"]
            temp_comment["conflict_type"] = "GRAMMAR_SPELLING"
            temp_comment["comment_id"] = comment_dict["comment_id"]
            temp_comment["reference_doc"] = comment_dict["reference_doc"]
            temp_comment["file_id"] = file_id
            temp_comment["target_text"] = target_text
            temp_comment["project_id"] = project_id
            temp_comment["updated_at"] = None
            dt = datetime.datetime.now().isoformat()
            temp_comment["created_at"] = dt
            temp_comment["action"] = None
            temp_comment["action_on"] = None
            temp_comment["action_by"] = None
            temp_comment["_deleted"] = False
            error_conflicts_comments.append(temp_comment)
        count=count+1

    
    for correction in corrections['grammar']:       
        if correction['type_of_language_error']=='GRAMMAR_ERROR':
            comment_dict = {}
            comment = ""
            target_text = ""
            if len(correction['suggestions']):
                comment = "Type of error: {}.\nSuggestions: {}".format(correction['type_of_language_error'], ", ".join(correction["suggestions"]))
                comment_dict["target_text"] = correction["text"]
                comment_dict["comment_text"] = comment
                comment_dict["conflict_type"] = "GRAMMAR_SPELLING"
                comment_dict["suggestions"] = correction["suggestions"]
                comment_dict["comment_id"] = str(uuid.uuid1())
                comment_dict["reference_doc"] = ref_doc_path,
                comment_dict["type_of_language_error"] = correction["type_of_language_error"]
            else:
                comment = "Type of error: {}.\nNo suggestions".format(correction["type_of_language_error"])
                comment_dict["target_text"] = correction["text"]
                comment_dict["comment_text"] = comment
                comment_dict["conflict_type"] = "GRAMMAR_SPELLING"
                comment_dict["suggestions"] = []
                comment_dict["comment_id"] = str(uuid.uuid1())
                comment_dict["reference_doc"] = ref_doc_path,
                comment_dict["type_of_language_error"] = correction["type_of_language_error"]
            errors.append(comment_dict)
            if g_count==1:
                activeDoc, target_text = add_comment(activeDoc, correction["start"], correction["end"],comment)
            else:
                activeDoc, target_text =  add_comment(activeDoc, correction["start"]+g_count-1, correction["end"]+g_count-1,comment)
            temp_comment = {}
            temp_comment["recommendations"] = comment_dict["comment_text"]
            temp_comment["comment_text"] = comment_dict["comment_text"]
            temp_comment["conflict_type"] = "GRAMMAR_SPELLING"
            temp_comment["comment_id"] = comment_dict["comment_id"]
            temp_comment["reference_doc"] = comment_dict["reference_doc"]
            temp_comment["file_id"] = file_id
            temp_comment["target_text"] = target_text
            temp_comment["project_id"] = project_id
            temp_comment["updated_at"] = None
            dt = datetime.datetime.now().isoformat()
            temp_comment["created_at"] = dt
            temp_comment["action"] = None
            temp_comment["action_on"] = None
            temp_comment["action_by"] = None
            temp_comment["_deleted"] = False
            error_conflicts_comments.append(temp_comment)
        g_count=g_count+1         
    # if (len(error_conflicts_comments)):
        # print("-------------------")
        # object_id = ObjectId(project_id)
        # inserted = mycol.update_one({"_id": object_id}, {"$push" : {"conflicts.comments": error_conflicts_comments}})
        # mycol.find_and_modify()
        # inserted = mycol.insert_many(error_conflicts_comments)
        # print("Saved to database")
        # removes the ids
        # comment_ids = list(map(lambda x: x.pop("_id", None), error_conflicts_comments))    
    activeDoc.Save()

    doc.Close(False)
    word.Quit()
    return error_conflicts_comments
