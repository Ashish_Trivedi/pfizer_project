from font_formatting import font_formatter_v2
import json
import cherrypy
import pythoncom
import win32com.client as win32
from win32com.client import constants

import os
import random
import string
import os.path
from cherrypy.lib import static
from updated_order_conflict import order_conflict_generator
from updated_spell_check import spell_grammar_errors
import pprint
import pymongo
from accept_reject import apply_changes, delete_comments
from config import checklist_status_collection, database_name, collection_name, mongodb_uri, user_name, password, medical_data_filepath, conflict_collection_name
medical_data_filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", medical_data_filepath))

from bson.objectid import ObjectId

class WordConflictTest(object):
	@cherrypy.expose
	@cherrypy.tools.json_out()
	def index(self):
		cl = cherrypy.request.headers['Content-Length']
		rawbody = cherrypy.request.body.read(int(cl))
		body = json.loads(rawbody)
		lpd_filepath = body["label_filepath"]
		file_id = body["file_id"]
		ha_filepath = body["ha_filepath"][0]
		country_name = body["country_name"]
		project_id = body["project_id"]
		# JUST FOR THE HECK OF IT - ADDED extra field. PEACE :3
		checklist_filepath = body["checklist_filepath"]
		previousLabel_filepath = body["previousLabel_filepath"]
		fontFormat_filepath = body["fontFormat_filepath"][0]
		reference_filepath = body["reference_filepath"][0]

		check_list_dict = {
			"project_id": project_id,
			"file_id": file_id,
			"checks":[
			{"quality_check": "Is change applicable to approved indication / product in country?", "status" : False},
			{"quality_check": "Does updated text align with International and Regional Guidelines and Requirements?", "status": False},
			{"quality_check": "Are there any conflicts in the text and is terminology consistent?" , "status": False},
			{"quality_check": "Have any PCO requests been included?", "status": False},
			{"quality_check": "Hidden text  has been removed?", "status": False},
			{"quality_check": "Check consistency across strengths, forms, presentations and other products", "status": False}
			]
		}

		len_err_conflicts = 0
		len_order_conflicts = 0
		len_comments = 0

		print("before operations")
		err_conflicts = spell_grammar_errors(medical_data_filepath=medical_data_filepath,  lpd_filepath = lpd_filepath, ref_doc_path=None, file_id=file_id, project_id=project_id)
		print("spelling errors completed", len(err_conflicts))
		check_list_dict["checks"].append({"quality_check": "Spelling and grammar", "status": True})
		
		order_conflicts = order_conflict_generator(label_filepath=lpd_filepath, ha_filepath=ha_filepath, country_name=country_name, file_id=file_id, project_id=project_id)
		print("order conflicts completed", len(order_conflicts))
		check_list_dict["checks"].append({"quality_check": "Is wording and re-numbering accurate and updates in correct location?", "status": True})
		
		updated_doc_path, comments = font_formatter_v2(src_filepath=lpd_filepath, template_path=ha_filepath, country_name=country_name, reference_filepath = fontFormat_filepath, file_id=file_id, project_id=project_id)
		print("font conflicts completed", len(comments))
		check_list_dict["checks"].append({"quality_check": "Format, font and layout", "status": True})
		check_list_dict["checks"].append({"quality_check": "Punctuation, font size/type, spacing etc", "status": True})

		myclient = pymongo.MongoClient(mongodb_uri, username=user_name, password=password, authSource=database_name)
		mydb = myclient[database_name]
		mycol = mydb[checklist_status_collection]

		inserted_doc = mycol.insert_one(check_list_dict)
		print("Saved checklist status in db")

		len_err_conflicts = len(err_conflicts)
		len_order_conflicts = len(order_conflicts)
		len_comments = len(comments)
		total_len = len_comments + len_order_conflicts + len_err_conflicts
		# temp_order_conflicts = order_conflicts
		# temp_spell_conflicts = err_conflicts

		# temp_comments= []
		# for comment in comments:
		# 	temp_comments.append({"comment_id":comment["comment_id"], "accept": True})

		# print(json.dumps(temp_comments))
		# return {"response": True}

		total_errors = []
		total_errors.extend(err_conflicts)
		total_errors.extend(order_conflicts)
		total_errors.extend(comments)

		# product_label_col = mydb[collection_name]
		# object_id = ObjectId(project_id)
		# inserted = product_label_col.update_one({"_id": object_id}, {"$set" : {"conflicts.comments": order_conflicts}})

		conflicts_col = mydb[conflict_collection_name]
		inserted = conflicts_col.insert_many(total_errors)
		print("Saved to database")
		# removes the ids
		comment_ids = list(map(lambda x: x.pop("_id", None), total_errors))

		response = {}
		response["filepath"] = updated_doc_path
		response["font_comments"] = comments
		response["order_comments"] = order_conflicts
		response["err_comments"] = err_conflicts
		response["conflicts"] = {}
		response["conflicts"]["total"] = total_len
		response["conflicts"]["conflict_type"] = {}
		response["conflicts"]["conflict_type"]["font"] = len_comments
		response["conflicts"]["conflict_type"]["order"] = len_order_conflicts
		response["conflicts"]["conflict_type"]["spell_grammar"] = len_err_conflicts
		return response

		# print(conflicts)
		# try:
		# 	updated_doc_path, comments = font_formatter_v2(src_filepath=lpd_filepath, template_path=ha_filepath, country_name=country_name, reference_filepath = reference_filepath)
		# 	path = os.path.abspath(updated_doc_path)
			
		# 	response = {}
		# 	response["filepath"] = path
		# 	response["comments"] = comments
		# 	response["conflicts"] = {}
		# 	response["conflicts"]["total"] = len(comments)
		# 	response["conflicts"]["conflict_type"] = {}
		# 	response["conflicts"]["conflict_type"]["font"] = len(comments)
		# 	return response
		# except Exception as e:
		# 	err_response = {}
		# 	err_response["error"] = True
		# 	err_response["message"] = "Internal server error"
		# 	err_response["stacktrace"] = str(e)
		# 	return err_response
		# static.serve_file(path, 'application/x-download','attachment', os.path.basename(path))

	@cherrypy.expose
	@cherrypy.tools.json_out()
	@cherrypy.tools.json_in()
	def accept(self):
		input_json = cherrypy.request.json
		if "comments" not in input_json:
			raise cherrypy.HTTPError(400, message="Bad request, missing field comment_id in the body")
		if "file_id" not in input_json:
			raise cherrypy.HTTPError(400, message="Bad request, missing field file_id in the body")
		if "label_filepath" not in input_json:
			raise cherrypy.HTTPError(400, message="Bad request, missing field label_filepath in the body")
		if "project_id" not in input_json:
			raise cherrypy.HTTPError(400, message="Bad request, missing field project_id in the body")

		label_filepath = input_json["label_filepath"]
		file_id = input_json["file_id"]
		project_id = input_json["project_id"]

		myclient = pymongo.MongoClient(mongodb_uri, username=user_name, password=password, authSource=database_name)
		mydb = myclient[database_name]
		# mycol = mydb[collection_name]
		comments = input_json["comments"]
		
		# acceptance
		accept_found_comments = []
		reject_found_comments = []
		for comment in comments:
			if comment["action"] == "ACCEPT":
				accept_found_comments.append(comment)
			elif comment["action"] == "REJECT":
				reject_found_comments.append(comment)


		# accept_found_comments = mycol.find({"comment_id": {"$in": accept_comment_ids}, "file_id": file_id})
		
		if not len(accept_found_comments) and not len(reject_found_comments):
			return {
				"message": "No comment exists"
			}
		else:
			word = win32.gencache.EnsureDispatch('Word.Application')
			word.Visible = False	
			doc = word.Documents.Open(label_filepath)
			activeDoc = word.ActiveDocument
			accept_found_comments_list = []
			for comment in accept_found_comments:
				accept_found_comments_list.append(comment)
				if comment["conflict_type"] == "FONT_SIZE":
					font_size = comment["recommendations"]["font_size"]
					activeDoc = apply_changes(word, doc, comment["start_index"], comment["end_index"], font_size, None)
				elif comment["conflict_type"] == "FONT_NAME":
					font_name = comment["recommendations"]["font_name"]
					activeDoc = apply_changes(word, doc, comment["start_index"], comment["end_index"], None, font_name)
			
			# print(accept_found_comments_list)
			activeDoc = delete_comments(activeDoc, accept_found_comments_list)
			
			# reject_found_comments = mycol.find({"comment_id" : {"$in": reject_comment_ids}, "file_id": file_id})
			
			reject_found_comments_list = []
			for comment in reject_found_comments:
				reject_found_comments_list.append(comment)
			activeDoc = delete_comments(activeDoc, reject_found_comments_list)

			activeDoc.Save()
			doc.Close(False)
			word.Quit()

			checklist_col = mydb[checklist_status_collection]
			my_query = {"file_id": file_id, "project_id": project_id}
			my_new_values = {"$set": {"checks.0.status": True}}
			new_vals = {"$set": {"checks.1.status": True}}
			updated_doc = checklist_col.update_one(my_query, my_new_values)
			updated_doc = checklist_col.update_one(my_query, new_vals)

			return {"label_filepath": label_filepath}



def onThreadStart(threadIndex):
		pythoncom.CoInitialize()

cherrypy.config.update({'server.socket_port': 3001})
cherrypy.config.update({
    'global': {
       'engine.autoreload.on' : False,
		'server.socket_host': '0.0.0.0',
		'server.thread_pool' :1
     }
 })

if __name__ == '__main__':
	cherrypy.engine.subscribe('start_thread', onThreadStart, )
	cherrypy.quickstart(WordConflictTest(), '/')