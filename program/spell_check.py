import win32com.client
import pickle
from win32com.client import constants

def add_comment(word, doc, start_index,end_index, comment_text):
    print("Adding commment for spell and grammar at index {}".format(start_index))
    doc.Activate()
    conflict_range = doc.Range(start_index,end_index)
    _ = doc.Comments.Add(conflict_range, comment_text)        
    return doc
def Medical_term(d,string):
    if string in d: 
        return True
    else:
        return False

def rng_to_spell(rng):
        d = {'text': rng.Text, 'start': rng.Start, 'end': rng.End, 'suggestions': [],'type_of_language_error':'SPELL_ERROR'}
        for i in range(1, rng.GetSpellingSuggestions().Count+1):
            d['suggestions'].append(rng.GetSpellingSuggestions().Item(i).Name)
        return d   
def spell_grammar_errors(lpd_filepath):
    word = win32com.client.Dispatch("Word.Application")
    doc = word.Documents.Open(lpd_filepath)
    error_dict={}
    error_list=[]
    Before_med_term=0
    After_med_term=0
    corrections = { 'spelling': [],'spellingcount': 0 }
    with open('medical_terms.pkl', 'rb') as medict:
        Medical_data = pickle.load(medict)
    Before_med_term=doc.Range().SpellingErrors.Count
    # print(Before_med_term)
    for rng in doc.Range().SpellingErrors:
#         Before_med_term=doc.Range().SpellingErrors.Count
        Check_med_word= Medical_term(Medical_data,rng.Text)
        if Check_med_word!=True:
            After_med_term=After_med_term+1
            corrections['spelling'].append(rng_to_spell(rng))
            error_list.append(rng.Text)  

    count=1
    errors=[]
    for correction in corrections['spelling']:
        if correction['type_of_language_error']=='SPELL_ERROR':
            comment = ""
            comment_dict={}
            if len(correction['suggestions']):
                comment = "Type of language error: {}.\nSuggestions: {}".format(correction['type_of_language_error'], ", ".join(correction["suggestions"]))
                comment_dict["target_text"] = correction["text"]
                comment_dict["comment_text"] = comment
                comment_dict["conflict_type"] = "GRAMMAR_SPELLING"
                comment_dict["suggestions"] = correction["suggestions"]
                comment_dict["type_of_language_error"] = correction["type_of_language_error"]
            else:
                comment = "Type of language error: {}.\nNo suggestions".format(correction["type_of_language_error"])
                comment_dict["target_text"] = correction["text"]
                comment_dict["comment_text"] = comment
                comment_dict["conflict_type"] = "GRAMMAR_SPELLING"
                comment_dict["suggestions"] = []
                comment_dict["type_of_language_error"] = correction["type_of_language_error"]
            errors.append(comment_dict)
            if count==1:
                add_comment(word, doc, correction["start"], correction["end"],comment)
            else:
                add_comment(word, doc, correction["start"]+count-1, correction["end"]+count-1,comment)
        count=count+1
    # word.Quit()
    return errors           


