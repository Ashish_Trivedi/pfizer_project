import json
import cherrypy
import pythoncom
import comtypes.client
import os
import random
import string
import os.path
# from mapping_gen import mapping_gen
from mapping_gen_v2 import mapping_gen
import gensim

## Download the pubmed word2vec from
#   https://drive.google.com/uc?export=download&confirm=JWbK&id=0BzMCqpcgEJgiUWs0ZnU0NlFTam8

embedding_path = "C:\\Users\\Reeth\\Desktop\\prod_font_comments\\formatting-comments\\bio_nlp_vec\\PubMed-shuffle-win-30.bin"
word2vec_model = gensim.models.KeyedVectors.load_word2vec_format(embedding_path, binary=True, limit=800000)
word2vec_model.init_sims(replace = True) # normalizes vectors

class MappingGenerator(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def index(self):
        if cherrypy.request.method == "POST":
            label_filepath = cherrypy.request.json['label_filepath']
            uspi_filepath = cherrypy.request.json["reference_filepath"][0]
            result = mapping_gen(label_filepath, uspi_filepath, word2vec_model)
            # print(result)
            return result


def onThreadStart(threadIndex):
		pythoncom.CoInitialize()

cherrypy.config.update({'server.socket_port': 3000})
cherrypy.config.update({
    'global': {
       'engine.autoreload.on' : False,
       'server.socket_host': '0.0.0.0'
     }
 })

if __name__ == '__main__':
	cherrypy.engine.subscribe('start_thread', onThreadStart, )
	cherrypy.quickstart(MappingGenerator(), '/')