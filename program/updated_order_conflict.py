import tika.parser as parser
import re
import pandas as pd
from docx.api import Document
import numpy as np
import difflib
from itertools import chain
import zipfile
import xml.dom.minidom
import xml.etree.ElementTree
import json
import win32com.client as win32
from win32com.client import constants
import collections
import uuid
import datetime
import pymongo
from config import mongodb_uri, database_name, collection_name, user_name, password
from bson.objectid import ObjectId

def read_file(path):
    new = parser.from_file(path)
    text = new['content']
    return text
def trim_doc(file_text, start, end):
    file_text = file_text[(file_text.index(start)+len(start)):file_text.index(end)]
    return file_text
def split_on_letter(s):
    match = re.compile("[^\W\d]").search(s)
    return [s[:match.start()], s[match.start():]]
def section_builder(temp_sect, split_section):
    for i in range(len(temp_sect)-1):
        if(int(temp_sect[i+1][0])-int(temp_sect[i][0])==1):
            pass
        else:
            missing_num = int(temp_sect[i+1][0])-int(temp_sect[i][0])
            for k in range(1,missing_num):
                for item in split_section:
                    if(int(item[0])==int(temp_sect[i][0])+k):
                        temp_sect.append(item)
    return temp_sect

def template_extraction(path_tempelate, country_name):
    if(country_name =='Bosnia'):
        start1 = 'This will allow quick identification of \nnew safety information. Healthcare professionals are asked to report any suspected adverse reactions. \nSee section 4.8 for how to report adverse reactions.'
        end1 = '\n \n \n1. NAME OF THE MEDICINAL PRODUCT \n \n{(Invented) name strength pharmaceutical form} [as it appears in the SmPC under section 1.] \n{Active substance(s)}  \n \n[The reference to the active substance should correspond to the strength expressed in the name'
    elif(country_name == 'Saudi Arabia'):
        start1 = '[ADD: Black Box Warning if applicable] \n'
        end1 = '\n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n\n\n \n\n19 \n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\n \n\nLABELLING'
    temp_text1 = trim_doc(read_file(path_tempelate),start1,end1)
    #section_extraction
    reg = re.compile('\n(\d{1,2}\. .*?)\n')
    temp_section = reg.findall(temp_text1)
    temp_section = [re.sub(r'\s+', ' ', i).strip() for i in temp_section]
    #subsection_extraction
    reg1 = re.compile('\n(\d{1}\.\d{1}[\n ].*?)\n')
    temp_subsection = reg1.findall(temp_text1)
    temp_subsection = [re.sub(r'\s+', ' ',i).strip() for i in temp_subsection]
    #mixed_section_subsection
    reg2 = re.compile('\n(\d{1,2}\. .*?)\n|\n(\d{1}\.\d{1}[\n ].*?)\n|(\d{1}\.\d{1}\n[A-za-z].*?)\n')
    temp_label = reg2.findall(temp_text1)
    updated_temp_label = []
    for tple in temp_label:
        tple = [t for t in tple if t]
        updated_temp_label.append(tple[0])
    updated_temp_label = [re.sub(r'\s+', ' ', i).strip()for i in updated_temp_label]
    return updated_temp_label, temp_section, temp_subsection

def lpd_extraction(path_lpd,path_tempelate,country_name):
    text = read_file(path_lpd)
    reg_comment = re.compile(r"Comment by.*")
    found_comments = reg_comment.findall(text)
    #found_comments 
    formatted_comments = []
    for comment in found_comments:
        formatted_comments.append(comment.strip())
    for comment in formatted_comments:
        text = text.replace("\t"+comment, "")
    if(country_name =='Bosnia'):
        #section_extraction
        reg = re.compile('\n\n(\d{1,2}\. .*?)\n')
        section = reg.findall(text)
        section_list =[re.sub(r'\s+', ' ',i).strip() for i in section]
        #subsection_extraction
        reg1 = re.compile("\n\n(\d{1}\.\d{1} .*?)\n")
        subsection = reg1.findall(text)
        subsection_list =[re.sub(r'\s+', ' ',i).strip() for i in subsection]
        #mixed section_subsection
        reg2 = re.compile('\n\n(\d{1,2}\. .*?)\n|\n\n(\d{1}\.\d{1} .*?)\n|\n(\d{1}\.\d{1} .*?)\n')
        label = reg2.findall(text)
        updated_label = []
        for tple in label:
            tple = [t for t in tple if t]
            updated_label.append(tple[0])
        label_list = [re.sub(r'\s+', ' ', i).strip()for i in updated_label] 
    elif(country_name == 'Saudi Arabia'):
        #section_extraction
        reg = re.compile('\t*\n(\d{1}\.[\n\t ]*[A-Za-z]+.*?)\n')
        section = reg.findall(text)
        sections =[re.sub(r'\s+', ' ',i).strip() for i in section]
        split_section = [i.split(".") for i in sections]
        split_section = [[re.sub(r'\s+', ' ', i[0]).strip(), re.sub(r'\s+', ' ', i[1]).strip()] for i in split_section]
        temp_mixed_list, temp_section, temp_subsection = template_extraction(path_tempelate, country_name)
        split_temp_section = []
        for s in temp_section:
            split_temp_section.append(split_on_letter(s))
        split_temp_section = [[re.sub(r'\s+', ' ', i[0]).strip(),re.sub(r'\s+', ' ', i[1]).strip()] for i in split_temp_section]
        sect = []
        for j in split_section:
            for k in split_temp_section:
                if j[1].lower() in k[1].lower():
                    sect.append(j)
        sect = section_builder(sect, split_section)
        for i in sect:
            i[0] = i[0]+'.'
        sect = sorted(sect, key=lambda x: float(x[0]))
        section_list = [i[0]+i[1] for i in sect] 
        #subsection_extraction
        reg1 = re.compile(r"(?m)^\n*\d+(?:\.\d+\.?)+[ ]*[\t\n][A-Za-z]+\S.*$")
        subsection = reg1.findall(text)
        subsection1 = [line.strip('\n') for line in subsection]
        subsections =[re.sub(r'\s+', ' ',i).strip() for i in subsection]
        split_subsection=[]
        for s in subsections:
            split_subsection.append(split_on_letter(s))
        split_subsection = [[re.sub(r'\s+', ' ', i[0]).strip(), re.sub(r'\s+', ' ', i[1]).strip()] for i in split_subsection]
        split_subsection_text =[i[1].lower() for i in split_subsection]
        split_temp_subsection=[]
        for s in temp_subsection:
            split_temp_subsection.append(split_on_letter(s))
        split_temp_subsection = [[re.sub(r'\s+', ' ', i[0]).strip(),re.sub(r'\s+', ' ', i[1]).strip()] for i in split_temp_subsection]
        for i in split_temp_subsection:
            if i[1].lower() not in split_subsection_text and i[1].lower() in text.lower():
                    #print("in Text1.lower",i[1].lower())
                    split_subsection.append(i)
        split_subsection = sorted(split_subsection, key=lambda x: float(x[0].strip('.')))
        subsection_list = [i[0]+i[1] for i in split_subsection]
        #mixed_section_subsection
        flat_list = [sect,split_subsection]
        mixed_list = [item for sublist in flat_list for item in sublist]
        mixed_list =sorted(mixed_list, key=lambda x: float(x[0].strip('.')))
        label_list = [" ".join(i) for i in mixed_list]
    return label_list,section_list,subsection_list

def paragraph_extractor(path_lpd):
    word = win32.gencache.EnsureDispatch('Word.Application')
    word.Visible = True
    doc = word.Documents.Open(path_lpd)

    indices = []
    paras = []
    for idx, para in enumerate(doc.Paragraphs):
        if len(str(para).strip()) > 0:
            index = idx+1
            indices.append(index)
            text = str(para)
            text = text.replace("\r", "").strip()
            paras.append(text)

    paragraphs_dict = {
        "indices": indices,
        "text": paras
    }
    doc.Close(False)
    word.Quit() 
    return paragraphs_dict

def matching_percentage(a, b):
    diffs = sum([i[0] != ' '  for i in difflib.ndiff(a, b)])
    total = 0
    if len(a) > len(b):
        total = len(a)
    else:
        total = len(b)

    percentage = 1.0 - diffs/total
    return percentage

def my_comparator(row, text, threshold=0.9):
    m_percent = matching_percentage(row["text_i"], text)
    if m_percent > 0.9:
        return m_percent
    else:
        return np.nan

def ordered_labels(indices_df, mixed_list):
    index_dict = {}
    for i in mixed_list:
        list_item = i.strip().replace(" ", "").lower()
        for idx, text in enumerate(indices_df["text_i"]):
            if matching_percentage(list_item, text) > 0.7:
                index = indices_df["indices"][idx]
                index_dict[index] = i
    od = collections.OrderedDict(sorted(index_dict.items()))
    return od

def updated_order_conflict(label_order_dict, list2, label_filepath,ref_doc_path, file_id, project_id):
    myclient = pymongo.MongoClient(mongodb_uri, username=user_name, password=password, authSource=database_name)
    mydb = myclient[database_name]
    mycol = mydb[collection_name]
    print("yes")


    word = win32.gencache.EnsureDispatch('Word.Application')
    word.Visible = True
    doc = word.Documents.Open(label_filepath)
    doc.Activate()
    activeDoc = word.ActiveDocument
    content_order_conflict = []
    list1 = list(label_order_dict.values())
    index_label_dict = dict((v,k) for k,v in label_order_dict.items())
    #print(list1)
    #print(list2)
    s1 = [i.lower().replace(" ","") for i in list1]
    s2 = [i.lower().replace(" ","") for i  in list2]
    matcher = difflib.SequenceMatcher(None, s1, s2)
    content_order_conflict = []
    order_conflicts_comments = []
    for tag, i1, i2, j1, j2 in (matcher.get_opcodes()):
        outcome={}
        if tag == 'delete':
            list_i = list1[i1:i2]
            list_j = list2[j1:j2]
            for index in range(len(list_i)):
                print("@@@@@@@@@@@@@@@@@",type(index_label_dict[list_i[index]]))
                outcome =  {"comment_text": tag.upper() +":"+ list_i[index],
                            "conflict_type": "order",
                            "comment_id": str(uuid.uuid1()),
                            "reference_doc": ref_doc_path,
                            "index":int( index_label_dict[list_i[index]])
                        }
                activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                temp_comment = {}
                temp_comment["recommendations"] = outcome["comment_text"]
                temp_comment["comment_text"] = outcome["comment_text"]
                temp_comment["conflict_type"] = "ORDER"
                temp_comment["comment_id"] = outcome["comment_id"]
                temp_comment["reference_doc"] = outcome["reference_doc"]
                temp_comment["start_index"] = outcome["index"]
                temp_comment["end_index"] = outcome["index"]
                temp_comment["file_id"] = file_id
                temp_comment["target_text"] = target_text
                temp_comment["project_id"] = project_id
                temp_comment["updated_at"] = None
                dt = datetime.datetime.now().isoformat()
                temp_comment["created_at"] = dt
                temp_comment["action"] = None
                temp_comment["action_on"] = None
                temp_comment["action_by"] = None
                temp_comment["_deleted"] = False
                order_conflicts_comments.append(temp_comment)
                content_order_conflict.append(outcome)
        elif tag == 'insert':
            print('insert',list2[j1:j2])
            list_i = list1[i1:i2]
            list_j = list2[j1:j2]
            for index in range(len(list_j)):
                print("@@@@@@@@@@@@@@@@@@@@@@@",type(index_label_dict[list1[i1]]))
                outcome =  {"comment_text": tag.upper() +":"+ list_j[index],
                            "conflict_type": "order",
                            "comment_id": str(uuid.uuid1()),
                            "reference_doc": ref_doc_path,
                            "index": int(index_label_dict[list1[i1]])
                        }
                activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                temp_comment = {}
                temp_comment["recommendations"] = outcome["comment_text"]
                temp_comment["comment_text"] = outcome["comment_text"]
                temp_comment["conflict_type"] = "ORDER"
                temp_comment["comment_id"] = outcome["comment_id"]
                temp_comment["reference_doc"] = outcome["reference_doc"]
                temp_comment["start_index"] = outcome["index"]
                temp_comment["end_index"] = outcome["index"]
                temp_comment["file_id"] = file_id
                temp_comment["target_text"] = target_text
                temp_comment["project_id"] = project_id
                temp_comment["updated_at"] = None
                dt = datetime.datetime.now().isoformat()
                temp_comment["created_at"] = dt
                temp_comment["action"] = None
                temp_comment["action_on"] = None
                temp_comment["action_by"] = None
                temp_comment["_deleted"] = False
                order_conflicts_comments.append(temp_comment)
                content_order_conflict.append(outcome)
            print(list1)
        elif tag == 'replace':
            list_i = list1[i1:i2]
            list_j = list2[j1:j2]
            if(len(list_i)==len(list_j)):
                for index in range(len(list_i)):
                    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",type(index_label_dict[list_i[index]]))
                    outcome =  {"comment_text": tag.upper() +" WITH:"+ list_j[index],
                                "conflict_type": "order",
                                "comment_id": str(uuid.uuid1()),
                                "reference_doc": ref_doc_path,
                                "index": int(index_label_dict[list_i[index]])
                                
                    }
                    activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                    temp_comment = {}
                    temp_comment["recommendations"] = outcome["comment_text"]
                    temp_comment["comment_text"] = outcome["comment_text"]
                    temp_comment["conflict_type"] = "ORDER"
                    temp_comment["comment_id"] = outcome["comment_id"]
                    temp_comment["reference_doc"] = outcome["reference_doc"]
                    temp_comment["start_index"] = outcome["index"]
                    temp_comment["end_index"] = outcome["index"]
                    temp_comment["file_id"] = file_id
                    temp_comment["target_text"] = target_text
                    temp_comment["project_id"] = project_id
                    temp_comment["updated_at"] = None
                    dt = datetime.datetime.now().isoformat()
                    temp_comment["created_at"] = dt
                    temp_comment["action"] = None
                    temp_comment["action_on"] = None
                    temp_comment["action_by"] = None
                    temp_comment["_deleted"] = False
                    order_conflicts_comments.append(temp_comment)
                    content_order_conflict.append(outcome)
            elif(len(list_i)<len(list_j)):
                for index in range(len(list_i)):
                    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",type(index_label_dict[list_i[index]]))
                    outcome =  {"comment_text": tag.upper() +" WITH:"+ list_j[index],
                                "conflict_type": "order",
                                "comment_id": str(uuid.uuid1()),
                                "reference_doc": ref_doc_path,
                                "index": int(index_label_dict[list_i[index]])
                    }
                    activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                    temp_comment = {}
                    temp_comment["recommendations"] = outcome["comment_text"]
                    temp_comment["comment_text"] = outcome["comment_text"]
                    temp_comment["conflict_type"] = "ORDER"
                    temp_comment["comment_id"] = outcome["comment_id"]
                    temp_comment["reference_doc"] = outcome["reference_doc"]
                    temp_comment["start_index"] = outcome["index"]
                    temp_comment["end_index"] = outcome["index"]
                    temp_comment["file_id"] = file_id
                    temp_comment["target_text"] = target_text
                    temp_comment["project_id"] = project_id
                    temp_comment["updated_at"] = None
                    dt = datetime.datetime.now().isoformat()
                    temp_comment["created_at"] = dt
                    temp_comment["action"] = None
                    temp_comment["action_on"] = None
                    temp_comment["action_by"] = None
                    temp_comment["_deleted"] = False
                    order_conflicts_comments.append(temp_comment)
                    content_order_conflict.append(outcome)
                for left in range((len(list_j)-len(list_i))):
                    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",type(index_label_dict[list_i[len(list_i)-1]]))
                    outcome = {"comment_text": "insert".upper() +":"+ list_j[len(list_i)+left],
                               "conflict_type": "order",
                               "comment_id": str(uuid.uuid1()),
                                "reference_doc": ref_doc_path,
                               "index": int(index_label_dict[list_i[len(list_i)-1]])
                    }
                    activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                    temp_comment = {}
                    temp_comment["recommendations"] = outcome["comment_text"]
                    temp_comment["comment_text"] = outcome["comment_text"]
                    temp_comment["conflict_type"] = "ORDER"
                    temp_comment["comment_id"] = outcome["comment_id"]
                    temp_comment["reference_doc"] = outcome["reference_doc"]
                    temp_comment["start_index"] = outcome["index"]
                    temp_comment["end_index"] = outcome["index"]
                    temp_comment["file_id"] = file_id
                    temp_comment["target_text"] = target_text
                    temp_comment["project_id"] = project_id
                    temp_comment["updated_at"] = None
                    dt = datetime.datetime.now().isoformat()
                    temp_comment["created_at"] = dt
                    temp_comment["action"] = None
                    temp_comment["action_on"] = None
                    temp_comment["action_by"] = None
                    temp_comment["_deleted"] = False
                    order_conflicts_comments.append(temp_comment)
                    content_order_conflict.append(outcome)
            elif(len(list_i)>len(list_j)):
                for index in range(len(list_j)):
                    print("@@@@@@@@@@@@@@@@@@@@",type(index_label_dict[list_i[index]]))
                    outcome =  {"comment_text": tag.upper() +" WITH:"+ list_j[index],
                                "conflict_type": "order",
                                "comment_id": str(uuid.uuid1()),
                                "reference_doc": ref_doc_path,
                                "index":int(index_label_dict[list_i[index]])
                    }
                    activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                    temp_comment = {}
                    temp_comment["recommendations"] = outcome["comment_text"]
                    temp_comment["comment_text"] = outcome["comment_text"]
                    temp_comment["conflict_type"] = "ORDER"
                    temp_comment["comment_id"] = outcome["comment_id"]
                    temp_comment["reference_doc"] = outcome["reference_doc"]
                    temp_comment["start_index"] = outcome["index"]
                    temp_comment["end_index"] = outcome["index"]
                    temp_comment["file_id"] = file_id
                    temp_comment["target_text"] = target_text
                    temp_comment["project_id"] = project_id
                    temp_comment["updated_at"] = None
                    dt = datetime.datetime.now().isoformat()
                    temp_comment["created_at"] = dt
                    temp_comment["action"] = None
                    temp_comment["action_on"] = None
                    temp_comment["action_by"] = None
                    temp_comment["_deleted"] = False
                    order_conflicts_comments.append(temp_comment)
                    content_order_conflict.append(outcome)
                for left in range((len(list_i)-len(list_j))):
                    print("@@@@@@@@@@@@@@@@@@",type(index_label_dict[list_i[len(list_j)+left]]))
                    outcome = {"comment_text": "Delete".upper() +":"+ list_i[len(list_j)+left],
                               "conflict_type": "order",
                               "comment_id": str(uuid.uuid1()),
                               "reference_doc": ref_doc_path,
                               "index": int(index_label_dict[list_i[len(list_j)+left]])
                    }
                    activeDoc, target_text = add_comment(activeDoc, outcome["index"], outcome["comment_text"])
                    temp_comment = {}
                    temp_comment["recommendations"] = outcome["comment_text"]
                    temp_comment["comment_text"] = outcome["comment_text"]
                    temp_comment["conflict_type"] = "ORDER"
                    temp_comment["comment_id"] = outcome["comment_id"]
                    temp_comment["reference_doc"] = outcome["reference_doc"]
                    temp_comment["start_index"] = outcome["index"]
                    temp_comment["end_index"] = outcome["index"]
                    temp_comment["file_id"] = file_id
                    temp_comment["target_text"] = target_text
                    temp_comment["project_id"] = project_id
                    temp_comment["updated_at"] = None
                    dt = datetime.datetime.now().isoformat()
                    temp_comment["created_at"] = dt
                    temp_comment["action"] = None
                    temp_comment["action_on"] = None
                    temp_comment["action_by"] = None
                    temp_comment["_deleted"] = False
                    order_conflicts_comments.append(temp_comment)
                    content_order_conflict.append(outcome)
    
    # if (len(order_conflicts_comments)):
        # inserted = mycol.insert_many(order_conflicts_comments)
        # object_id = ObjectId(project_id)
        # inserted = mycol.update_one({"_id": object_id}, {"$push" : {"conflicts.comments": order_conflicts_comments}})
        # print("Saved to database")
		# removes the ids
        # comment_ids = list(map(lambda x: x.pop("_id", None), order_conflicts_comments))
    
    activeDoc.Save()
    doc.Close(False)
    word.Quit()
    return order_conflicts_comments

def add_comment(activeDoc, index, comment_text):
    print("Adding comment - order conflict at index : {}".format(index))
    doc_range = activeDoc.Paragraphs(index).Range
    _ = activeDoc.Comments.Add(doc_range, comment_text)
    return activeDoc, comment_text


def order_conflict_generator(label_filepath, ha_filepath, country_name, file_id, project_id):
    template_mixed_list,b,c = template_extraction(ha_filepath, country_name)
    mixed_list,e,f = lpd_extraction(label_filepath, ha_filepath, country_name)
    paragraphs_dict = paragraph_extractor(label_filepath)
    indices_df = pd.DataFrame(paragraphs_dict)
    indices_df["text_i"] = indices_df["text"].apply(lambda row: row.strip().replace(" ", "").replace("\t", "").lower())
    label_order_dict = ordered_labels(indices_df, mixed_list)
    order_conflicts = updated_order_conflict(label_order_dict, template_mixed_list, label_filepath,ha_filepath,file_id,project_id)
    return order_conflicts

