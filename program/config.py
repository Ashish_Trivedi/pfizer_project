mongodb_uri = "mongodb://ds125273.mlab.com:25273"
database_name = "labellingdatabase"
collection_name = "productlabels"
conflict_collection_name = "conflicts"
user_name = "pfizeruser"
password = "qwerty1234"
medical_data_filepath = "medical_terms.pkl"
checklist_status_collection = "checklist_status"

'''
Is change applicable to approved indication / product in country? -y (Accept/Reject)
Is wording and re-numbering accurate and updates in correct location? -y (Order Conflict)
Are there any conflicts in the text and is terminology consistent? -y (Content Conflicts)
Have any PCO requests been included? -y (Add_comment_for_PCO)
Does updated text align with International and Regional Guidelines and Requirements? -y (Accept/Reject)
Spelling and grammar   -y (spell_check)
Format, font and layout -y (font format check)
Punctuation, font size/type, spacing etc - y (font format check)
Hidden text  has been removed -? (tbd)
Check consistency across strengths, forms, presentations and other products - y
'''