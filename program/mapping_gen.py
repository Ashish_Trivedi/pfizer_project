import pandas as pd
import json
import re
import nltk, string, numpy
nltk.download('punkt')
nltk.download('wordnet')
from sklearn.feature_extraction.text import TfidfVectorizer
import gensim
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk import download
download('stopwords') 
import os
import tika.parser as parser
import numpy as np
import difflib
from itertools import chain


def read_file(path):
    new = parser.from_file(path)
    text = new['content']
    return text

def content(list_name,text):
    lst = []
    for i in range(len(list_name)):
        if i in range(len(list_name)-1):
            my_string = text[(text.index(list_name[i])+len(list_name[i])):text.index(list_name[i+1])]
        else: 
            my_string = text[(text.index(list_name[i])+len(list_name[i])):]
        lst.append(my_string)
    return lst

def list_todframe(listname,columnname):
    df = pd.DataFrame({columnname:listname})
    return df

def trim_doc(file_text, start, end):
    file_text = file_text[(file_text.index(start)+len(start)):file_text.index(end)]
    return file_text


def creating_lpd_doc(path_lpd):
    lpd_text = read_file(path_lpd)
    re1 = re.compile('\n(\d{1}\.\t.*?)\n|\n\n(\d{1}\.\d{1}.*?)\n|\n(\d{1}\.\d{1}\t.*?)\n')
    lpd_labels = re1.findall(lpd_text)
    lpd_labels = list(chain.from_iterable(lpd_labels))
    lpd_labels = list(filter(None, lpd_labels))
   # print(lpd_labels)
    lpd_sections = [re.sub(r'\s+', ' ',i).strip() for i in lpd_labels]
    lpd_sections = list_todframe(lpd_sections, 'lpd_label')
    lpd_content_list = content(lpd_labels,lpd_text)
    lpd_with_content = [re.sub(r'\s+', ' ',i).strip() for i in lpd_content_list]
    lpd_with_content = list_todframe(lpd_with_content, 'lpd_content')
    lpd_data = pd.concat([lpd_sections, lpd_with_content], axis=1)
    return lpd_data



def creating_uspi_doc(uspi_path):
    start = 'Elderly patients and patients with a prior history of peptic ulcer disease and/or GI bleeding are at \ngreater risk for serious (GI) events. [see Warnings and Precautions (5.2)] \n\n \n\n'
    end = '\n \n\n \n \n\nLAB-0036-17.0 \nIssued: May 2016 \n \n\n\n\nMedication Guide for Nonsteroidal Anti-inflammatory Drugs (NSAIDs) '
    uspi_text = trim_doc(read_file(uspi_path),start,end)
    re1 = re.compile('\n(\d{1,2}\.\ .*?)\n|\n(\d{1,2}\.\d{1,2}\ [A-Z][^(].*?)\n')
    labels = re1.findall(uspi_text)
    labels = list(chain.from_iterable(labels))
    uspi_labels = list(filter(None, labels))
    uspi_sections = [re.sub(r'\s+', ' ',i).strip() for i in uspi_labels]
    uspi_sections = list_todframe(uspi_sections, 'uspi_label')
    uspi_content_list = content(uspi_labels, uspi_text)
    uspi_content_list = [re.sub(r'\s+', ' ',i).strip() for i in uspi_content_list]
    uspi_content_df = list_todframe(uspi_content_list, 'uspi_content')
    uspi_data = pd.concat([uspi_sections, uspi_content_df], axis=1)
    return uspi_data


# In[6]:


def label_wo_num(x):
    reg = re.compile("[A-Za-z]+")
    return " ".join(reg.findall(x))



def selector_func(row):
    if row["matching_score_content2"] > 1:
        if row["matching_score_content1"] < 0.3:
            return np.nan
        else:
            return row["predicted_mapping_content1"]
    else:
        return row["predicted_mapping_content2"]


def compute_score(row):
    if row["matching_score_content2"] > 1:
        if row["matching_score_content1"] < 0.3:
            final_score = (row['matching_score_content1'] - row['matching_score_content2_updated'])/2
            return final_score*100
        else:
            return row['matching_score_content1']*100
    else:
        final_score = (row['matching_score_content1'] + row['matching_score_content2_updated'])/2
        return final_score*100

def generalized_function(my_string):
    my_string = my_string.lower().split()
    stop_words = stopwords.words('english')
    my_string = [w for w in my_string if w not in stop_words]
    return my_string


def mapping_gen(label_filepath, uspi_filepath, word2vec_model):
    lpd = creating_lpd_doc(label_filepath)
    uspi_df = creating_uspi_doc(uspi_filepath)
    uspi_df["label_wo_num"] = uspi_df["uspi_label"].apply(label_wo_num)
    uspi_df["acc_text"] = uspi_df["uspi_label"].apply(label_wo_num) + " " + uspi_df["uspi_content"]
    lpd["label_wo_num"] = lpd["lpd_label"].apply(label_wo_num)
    lpd["acc_text"] = lpd["lpd_label"].apply(label_wo_num) + " " +lpd["lpd_content"]
    
    simlarities= pd.DataFrame()
    simlarities["lpd_Section"] = lpd["lpd_label"]
    lpd_text = lpd['label_wo_num'].apply(generalized_function)
    uspi_text = uspi_df['label_wo_num'].apply(generalized_function)
    
    predicted_mapping_content2 = []
    matching_score_content2 = []
    for i, lpd_section in enumerate(lpd_text):
        res_match = []
        for uspi_section in uspi_text:
            distance = word2vec_model.wmdistance(lpd_section, uspi_section)
            res_match.append(distance)
        Index = res_match.index(min(res_match))
        predicted_mapping_content2.append(uspi_df.loc[uspi_df.index[Index],"uspi_label"])
        matching_score_content2.append(min(res_match))
    
    simlarities['predicted_mapping_content2'] = predicted_mapping_content2
    simlarities['matching_score_content2'] = matching_score_content2
    
    simlarities['predicted_mapping_content2'] = simlarities.apply(lambda x: None if(x['matching_score_content2']>1) else x['predicted_mapping_content2'], axis = 1)
    raw_documents = uspi_df['acc_text'].tolist()
    
    gen_docs = []
    for text in raw_documents:
        temp_arr = []
        for w in word_tokenize(text):
            temp_arr.append(w.lower())
        gen_docs.append(temp_arr)
    
    dictionary = gensim.corpora.Dictionary(gen_docs)
    corpus = [dictionary.doc2bow(gen_doc) for gen_doc in gen_docs]
    tf_idf = gensim.models.TfidfModel(corpus)
    
    sims = gensim.similarities.Similarity('./',tf_idf[corpus],
                                      num_features=len(dictionary))
    
    predicted_mapping_content1 = []
    matching_score_content1 = []
    for lpd_section in lpd["acc_text"].tolist():
        query_doc = [w.lower() for w in word_tokenize(lpd_section)]
        query_doc_bow = dictionary.doc2bow(query_doc)
        query_doc_tf_idf = tf_idf[query_doc_bow]
        results = list(sims[query_doc_tf_idf])
        matching_score_content1.append(max(results))
        Index = results.index(max(results))
#         predicted_mapping_content1.append(uspi_df.iloc[results.index(max(results)),1])
        predicted_mapping_content1.append(uspi_df.loc[uspi_df.index[Index],'uspi_label'])
        
    simlarities['predicted_mapping_content1'] = predicted_mapping_content1
    simlarities['matching_score_content1'] = matching_score_content1
#     sample['PR'] = sample['PR'].apply(lambda x: np.nan if x < 90 else x)
    simlarities['matching_score_content2_updated'] = simlarities['matching_score_content2'].apply(lambda x: 1-x) 
    temp_df = simlarities[["lpd_Section","predicted_mapping_content1", "predicted_mapping_content2", "matching_score_content1", "matching_score_content2",'matching_score_content2_updated']]
    temp_df["final_matched"] = temp_df.apply(selector_func, axis=1)
    temp_df["confidence_score"] = temp_df.apply(compute_score, axis=1)
    final_json =  temp_df[["lpd_Section", "final_matched","confidence_score"]]
    # print(final_json)
    # print("*"*50)
    return final_json.to_json(orient="records")

